import buildUrl from 'build-url'
import { Event } from 'assets/types'

function mapLink(lat: number, lon: number, label: string, isGeoUrlSupported: boolean): string {
  if (isGeoUrlSupported) {
    return buildUrl(`geo:${lat},${lon}`, {
      queryParams: {
        q: label,
      },
    })
  } else {
    return buildUrl('https://www.google.com/maps', {
      queryParams: {
        q: `${label}@${lat},${lon}`,
      },
    })
  }
}

function mapLinkByEvent(event: Event, isGeoUrlSupported: boolean): string | undefined {
  let lat = event.venue.latitude
  let lon = event.venue.longitude
  if (!lat || !lon) {
    return undefined
  }
  return mapLink(lat, lon, event.venue.name, isGeoUrlSupported)
}

export { mapLink, mapLinkByEvent }
