let _items = []
const separator = '|'

function isColor(value) {
  return /^#[0-9a-f]{6}$/i.test(value.toString())
}

function toColor(value) {
  return '0x' + value.slice(1)
}

export default (json) => {
  _items.length = 0

  for (let i = 0; i < json.length; i++) {
    const item = json[i],
      hasFeature = item.hasOwnProperty('featureType'),
      hasElement = item.hasOwnProperty('elementType'),
      stylers = item.stylers

    let target = '',
      style = ''

    if (!hasFeature && !hasElement) {
      target = 'feature:all'
    } else {
      if (hasFeature) {
        target = 'feature:' + item.featureType
      }
      if (hasElement) {
        target = target ? target + separator : ''
        target += 'element:' + item.elementType
      }
    }

    for (let s = 0; s < stylers.length; s++) {
      let styleItem = stylers[s],
        key = Object.keys(styleItem)[0] // there is only one per element

      style = style ? style + separator : ''
      style += key + ':' + (isColor(styleItem[key]) ? toColor(styleItem[key]) : styleItem[key])
    }

    _items.push(target + separator + style)
  }
  return '&style=' + _items.join('&style=')
}
