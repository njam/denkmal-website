import Vue from 'vue'
import { VueMaskDirective } from 'v-mask'

export const initVMask = () => Vue.directive('mask', VueMaskDirective)
