import 'jasmine-expect'
import { getDenkmalApiUrl, todayDate } from '../helper/helpers'

describe('Date page of Friday', () => {
  let today = todayDate()
  let weekday = today.weekday
  let daysUntilFriday = weekday <= 5 ? 5 - weekday : 5 - weekday + 7
  let date = today.plus({ days: daysUntilFriday })

  beforeAll(() => {
    browser.url(`/de/basel/${date.toFormat('yyyy-MM-dd')}`)
    $('.page-region-date').waitForDisplayed()
  })

  it('has 7 lists of events', () => {
    expect($$('.DayEvents').length).toEqual(7)
  })

  it('has more than 1 event, each with infos', () => {
    let events = $$(`.swiper-slide:nth-child(${daysUntilFriday + 1}) .Event`)
    expect(events.length).toBeGreaterThan(1)
  })

  it('has an event with infos', () => {
    let $event = $(`.swiper-slide:nth-child(${daysUntilFriday + 1}) .DayEvents-list-item:first-child .Event`)
    expect($event.getAttribute('href')).toMatch(new RegExp(`/basel/${date.toFormat('yyyy-MM-dd')}/.+`))
    expect($event.$('.Event-meta-venue').getText()).toMatch(/\w{3,}/)
    expect($event.$('.Event-description').getText()).toMatch(/\w{3,}/)
  })

  it('has a weekday menu with 7 entries', () => {
    expect($$('.WeekdaysMenu .WeekdaysMenu-item').length).toEqual(7)
  })

  it('has a weekday menu that starts today', () => {
    let href = $('.WeekdaysMenu-item:first-child').getAttribute('href')
    expect(href).toMatch(new RegExp(`/basel/${today.toFormat('yyyy-MM-dd')}$`))
  })

  it('has no hint about out-of-week', () => {
    expect($$('.EventListDateHint').length).toEqual(0)
  })

  it('has Friday (short) in title', () => {
    expect(browser.getTitle()).toEqual('Fr - Denkmal.org')
  })
})

describe('Date page in the past', () => {
  let today = todayDate()
  let date = today.minus({ days: 2 })

  beforeAll(() => {
    browser.url(`/de/basel/${date.toFormat('yyyy-MM-dd')}`)
    $('.page-region-date').waitForDisplayed()
  })

  it('has 1 lists of events', () => {
    expect($$('.DayEvents').length).toEqual(1)
    expect($('.DayEvents').getAttribute('data-date')).toEqual(date.toFormat('yyyy-MM-dd'))
  })

  it('has no weekday menu', () => {
    expect($$('.WeekdaysMenu').length).toEqual(0)
  })

  describe('has a Home button', () => {
    it('has a hint and a link', () => {
      expect($$('.EventListDateHint').length).toEqual(1)
      expect($('.EventListDateHint a').getText()).toEqual('Home')
    })

    it('navigates back to the current week', () => {
      $('.EventListDateHint a').click()
      $('.WeekdaysMenu').waitForDisplayed()
      expect($('.DayEvents').getAttribute('data-date')).toEqual(today.toFormat('yyyy-MM-dd'))
      expect(browser.getUrl()).toEndWith(`/de/basel/${today.toFormat('yyyy-MM-dd')}`)
    })
  })
})

describe('Date page with invalid date', () => {
  beforeAll(() => {
    browser.allowUrl(getDenkmalApiUrl())
    browser.url(`/de/basel/2019-01-99`)
  })

  it('has an error message', () => {
    $('.page-error').waitForDisplayed()
    expect($('body').getText()).toContain("Invalid date: '2019-01-99'")
  })
})
