const slugify = require('@sindresorhus/slugify')
const NetworkWhitelist = require('./test/helper/wdio_network_whitelist')
const dnsSync = require('dns-sync')

let webdriverHost = process.env.WDIO_WEBDRIVER_HOST || 'localhost'
let webdriverPort = Number.parseInt(process.env.WDIO_WEBDRIVER_PORT || 4444)
let devtoolsIp = dnsSync.lookup(webdriverHost)
let devtoolsPort = 9222
let baseUrl = process.env.WDIO_BASEURL || 'http://localhost:3000'

console.log(`WDIO: webdriver address: ${webdriverHost}:${webdriverPort}`)
console.log(`WDIO: devtools address: ${devtoolsIp}:${devtoolsPort}`)
console.log(`WDIO: website URL: ${baseUrl}`)

function saveScreenshot(name) {
  name = slugify(name)
  let browserName = browser.capabilities.browserName
  let filename = `test/screenshots/${browserName}-${name}.png`
  browser.saveScreenshot(filename)
  console.log(`Screenshot saved in ${filename}`)
}

function clearStorage() {
  global.browser.execute(() => {
    window.localStorage.clear()
    window.sessionStorage.clear()
  })
}

exports.config = {
  runner: 'local',
  hostname: webdriverHost,
  port: webdriverPort,
  specs: ['./test/specs/**/*.js'],
  exclude: [],
  maxInstances: 1,
  capabilities: [
    {
      browserName: 'chrome',
      chromeOptions: {
        args: [
          '--headless',
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--window-size=320,600',
          `--remote-debugging-port=${devtoolsPort}`,
          '--remote-debugging-host=0.0.0.0',
        ],
      },
    },
  ],
  logLevel: 'warn',
  deprecationWarnings: true,
  //
  // If you only want to run your tests until a specific amount of tests have failed use
  // bail (default is 0 - don't bail, run all tests).
  bail: 0,
  baseUrl: baseUrl,
  //
  // Default timeout for all waitFor* commands.
  waitforTimeout: 10000,
  //
  // Default timeout in milliseconds for request
  // if Selenium Grid doesn't send response
  connectionRetryTimeout: 10000,
  connectionRetryCount: 3,
  services: [
    [
      'devtools',
      {
        debuggerAddress: `${devtoolsIp}:${devtoolsPort}`,
      },
    ],

    // We disable network access in tests, except for "baseUrl".
    // All data should be available in the generated pages already.
    // If a test needs network access, it must whitelist the URL with `allowUrl()`.
    [
      NetworkWhitelist,
      {
        allowedUrls: [baseUrl],
      },
    ],
  ],
  framework: 'jasmine',
  reporters: ['spec'],
  jasmineNodeOpts: {
    // Default time to wait in ms before a test fails.
    defaultTimeoutInterval: 20000,
    expectationResultHandler: function (passed, assertion) {
      if (!passed) {
        let timestamp = new Date().toJSON().replace(/:/g, '-')
        saveScreenshot(`ERROR-${timestamp}`)
      }
    },
  },

  //
  // =====
  // Hooks
  // =====
  // WebdriverIO provides several hooks you can use to interfere with the test process in order to enhance
  // it and to build services around it. You can either apply a single function or an array of
  // methods to it. If one of them returns with a promise, WebdriverIO will wait until that promise got
  // resolved to continue.
  /**
   * Gets executed once before all workers get launched.
   * @param {Object} config wdio configuration object
   * @param {Array.<Object>} capabilities list of capabilities details
   */
  // onPrepare: async function(config, capabilities) {}
  /**
   * Gets executed just before initialising the webdriver session and test framework. It allows you
   * to manipulate configurations depending on the capability or spec.
   * @param {Object} config wdio configuration object
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {Array.<String>} specs List of spec file paths that are to be run
   */
  beforeSession: function (config, capabilities, specs) {
    require('@babel/register')({
      presets: ['@babel/preset-env'],
    })
    require('@babel/polyfill')
  },
  /**
   * Gets executed before test execution begins. At this point you can access to all global
   * variables like `browser`. It is the perfect place to define custom commands.
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {Array.<String>} specs List of spec file paths that are to be run
   */
  // before: function(capabilities, specs) {}
  /**
   * Runs before a WebdriverIO command gets executed.
   * @param {String} commandName hook command name
   * @param {Array} args arguments that command would receive
   */
  // beforeCommand: function (commandName, args) {}
  /**
   * Hook that gets executed before the suite starts
   * @param {Object} suite suite details
   */
  beforeSuite: function (suite) {
    clearStorage()
  },
  /**
   * Function to be executed before a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
   * @param {Object} test test details
   */
  //beforeTest: function(test) {}
  /**
   * Hook that gets executed _before_ a hook within the suite starts (e.g. runs before calling
   * beforeEach in Mocha)
   */
  // beforeHook: function () {}
  /**
   * Hook that gets executed _after_ a hook within the suite starts (e.g. runs after calling
   * afterEach in Mocha)
   */
  // afterHook: function () {}
  /**
   * Function to be executed after a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
   * @param {Object} test test details
   */
  afterTest: function (test) {
    saveScreenshot(test.fullName)
  },
  /**
   * Hook that gets executed after the suite has ended
   * @param {Object} suite suite details
   */
  afterSuite: function (suite) {
    clearStorage()
  },
  /**
   * Runs after a WebdriverIO command gets executed
   * @param {String} commandName hook command name
   * @param {Array} args arguments that command would receive
   * @param {Number} result 0 - command success, 1 - command error
   * @param {Object} error error object if any
   */
  // afterCommand: function (commandName, args, result, error) {}
  /**
   * Gets executed after all tests are done. You still have access to all global variables from
   * the test.
   * @param {Number} result 0 - test pass, 1 - test fail
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {Array.<String>} specs List of spec file paths that ran
   */
  // after: function (result, capabilities, specs) {}
  /**
   * Gets executed right after terminating the webdriver session.
   * @param {Object} config wdio configuration object
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {Array.<String>} specs List of spec file paths that ran
   */
  // afterSession: async function(config, capabilities, specs) {}
  /**
   * Gets executed after all workers got shut down and the process is about to exit.
   * @param {Object} exitCode 0 - success, 1 - fail
   * @param {Object} config wdio configuration object
   * @param {Array.<Object>} capabilities list of capabilities details
   * @param {<Object>} results object containing test results
   */
  // onComplete: function(exitCode, config, capabilities, results) {}
}
